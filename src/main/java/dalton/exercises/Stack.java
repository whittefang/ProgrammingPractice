package dalton.exercises;

import java.util.ArrayList;
import java.util.List;

public class Stack {
	List<String> values;

	public Stack() {
		values = new ArrayList<String>();
	}

	public void push(String value) {
		values.add(value);
	}

	public String pop() {
		return values.remove(values.size()-1);
	}

	public String print() {
		String output = "";
		for (String value: values) {
			output = value + output;
		}
		return output;
	}

	public static void main(String[] args) {
		Stack stack = new Stack();

		stack.print();
		
		stack.push("1");
		stack.push("2");
		stack.push("3");
		System.out.println(stack.print());
		stack.pop();
		System.out.println(stack.print());
		stack.pop();
		System.out.println(stack.print());
		stack.pop();
		System.out.println(stack.print());

	}
}
