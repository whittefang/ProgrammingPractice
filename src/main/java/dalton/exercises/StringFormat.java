package dalton.exercises;

public class StringFormat {

	
	public static void main(String[] args) {
		System.out.println(replace("hello {0} my name is {1}", "justin", "funface"));
		System.out.println(replace("hello {0} my name is {1}", "justin", "dalton"));
	}
	
	static String replace(String base, String... replacements){
		int replacementNum = 0;
		
		for (String string : replacements) {
			String replacement = "\\{" + replacementNum + "\\}";
			 base = base.replaceFirst(replacement, string);
			replacementNum++;
		}
		
		return base;
		
	}
}
