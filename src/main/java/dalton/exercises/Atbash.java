package dalton.exercises;

import java.util.HashMap;
import java.util.Map;

public class Atbash {
	static Map<String, String> cypher;
	public static void main(String[] args) {
		cypher = new HashMap();
		cypher.put("a", "Z");
		cypher.put("b", "Y");
		cypher.put("c", "X");
		cypher.put("d", "W");
		cypher.put("e", "V");
		cypher.put("f", "U");
		cypher.put("g", "T");
		cypher.put("h", "S");
		cypher.put("i", "R");
		cypher.put("j", "Q");
		cypher.put("k", "P");
		cypher.put("l", "O");
		cypher.put("m", "N");
		cypher.put("n", "M");
		cypher.put("o", "L");
		cypher.put("p", "K");
		cypher.put("q", "J");
		cypher.put("r", "I");
		cypher.put("s", "H");
		cypher.put("t", "G");
		cypher.put("u", "F");
		cypher.put("v", "E");
		cypher.put("w", "D");
		cypher.put("x", "C");
		cypher.put("y", "B");
		cypher.put("z", "A");
		
		System.out.println(solve("irk"));
		
	}	
	
	static String solve(String input){
		String output = "";
		String[] chars = input.split("");
		for (String letter : chars) {
			output+= cypher.get(letter);
		}
		
		return output;
	}
}
