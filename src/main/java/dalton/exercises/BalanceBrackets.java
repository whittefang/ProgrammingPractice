package dalton.exercises;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class BalanceBrackets {
	static Map<String, String> bracketCompliments;
	public static void main(String[] args) {
		bracketCompliments = new HashMap<String, String>();
		bracketCompliments.put("(", ")");
		bracketCompliments.put("{", "}");
		bracketCompliments.put("[", "]");
		
		System.out.println(findBalanced("()[]{}"));
		System.out.println(findBalanced("([{}])"));
		System.out.println(findBalanced("([{])"));
		System.out.println(findBalanced("()[{}"));
		System.out.println(findBalanced(")[{}"));
	}
	
	static boolean findBalanced(String brackets){
		String[] tokens = brackets.split("");
		
		Stack<String> foundBrackets = new Stack<String>();
		for (String bracket : tokens) {
			String compliment = bracketCompliments.get(bracket);
			if (compliment == null){
				if (foundBrackets.size() > 0 && foundBrackets.peek().equals(bracket)){
					foundBrackets.pop();
				}else {
					return false;
				}
			}else if (compliment != null){
				foundBrackets.push(compliment);
			}
		}
		
		return foundBrackets.isEmpty();
	}
}
