package dalton.exercises;

import java.util.Arrays;
import java.util.List;

public class ArrayPairSum {
	public static void main(String[] args) {
		
		
		
		findPairs(10, Arrays.asList(new Integer[]{3,4,5,6,7}));
		
	}
	
	public static void findPairs(int sum, List<Integer> set){
		
		for(int i = 0; i < set.size(); i++){
			int currentPos =i;
			for(int ii=currentPos+1; ii < set.size(); ii++){
				if (set.get(i) + set.get(ii) == sum){
					System.out.println("found pair " + set.get(i) + " " + set.get(ii));
				}
			}
			
		}
		
		
	}
}
